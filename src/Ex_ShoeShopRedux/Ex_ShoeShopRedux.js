import React, { Component } from "react";
import { shoeArr } from "./data_shoeShop";
import ItemShoe from "./ItemShoe";
import ListShoe from "./Redux/reducer/ListShoe";
import TableGioHang from "./TableGioHang";

export default class Ex_ShoeShopRedux extends Component {
  state = {
    shoeArr: shoeArr,
    gioHang: [],
  };

  handleAddToCard = (sp) => {
    let indefx = this.state.gioHang.findIndex((item) => {
      return item.id == sp.id;
    });
    console.log("index", indefx);
    let cloneGioHang = [...this.state.gioHang];
    if (indefx == -1) {
      let newSp = { ...sp, soLuong: 1 };
      cloneGioHang.push(newSp);
    } else {
      cloneGioHang[indefx].soLuong++;
    }
    this.setState({ gioHang: cloneGioHang });
  };
  handleRemoveShoe = (idShoe) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idShoe;
    });
    if (index !== -1) {
      let cloneGioHang = [...this.state.gioHang];
      cloneGioHang.splice(index, 1);
      this.setState({ gioHang: cloneGioHang });
    }
  };
  handleChangeQuantity = (idShoe, step) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === idShoe;
    });
    let cloneGioHang = [...this.state.gioHang];
    cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + step;
    if (cloneGioHang[index].soLuong == 0) {
      cloneGioHang.splice(index, 1);
    }

    this.setState({ gioHang: cloneGioHang }, () => {
      console.log(this.state.gioHang.length);
    });
  };
  render() {
    return (
      <div className="container py-5">
        <TableGioHang
          handleRemoveShoe={this.handleRemoveShoe}
          gioHang={this.state.gioHang}
          handleChangeQuantity={this.handleChangeQuantity}
        />

        <ListShoe />
      </div>
    );
  }
}
