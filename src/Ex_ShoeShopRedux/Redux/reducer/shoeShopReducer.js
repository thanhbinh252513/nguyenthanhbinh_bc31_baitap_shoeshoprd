import { shoeArr } from "../../data_shoeShop";
import { ADD_TO_CART } from "./constant/shoeShopConstant";

let intialState = {
  shoeArr: shoeArr,
  gioHang: [],
};
export let shoeShopeReducer = (state = intialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let indefx = state.gioHang.findIndex((item) => {
        return item.id == payload.id;
      });
      // console.log("index", indefx);
      let cloneGioHang = [...state.gioHang];
      if (indefx == -1) {
        let newSp = { ...payload, soLuong: 1 };
        cloneGioHang.push(newSp);
      } else {
        cloneGioHang[indefx].soLuong++;
      }
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    default:
      return state;
  }
};
