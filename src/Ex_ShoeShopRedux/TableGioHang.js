import React, { Component } from "react";
import { connect } from "react-redux";

class TableGioHang extends Component {
  renderContent = () => {
    return this.props.cart.map((item) => {
      return (
        <tr key={item.id}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            {" "}
            <button
              className="btn btn-danger"
              onClick={() => this.props.handleChangeQuantity(item.id, -1)}
            >
              -
            </button>
            {item.soLuong}
            <button
              className="btn btn-warning"
              onClick={() => this.props.handleChangeQuantity(item.id, 1)}
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleRemoveShoe(item.id);
              }}
              className="btn btn-danger"
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>id</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderContent()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeShopeReducer.gioHang,
  };
};
export default connect(mapStateToProps)(TableGioHang);
